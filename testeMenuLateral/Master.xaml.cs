﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace testeMenuLateral
{
    public partial class Master : ContentPage
    {
        public Master()
        {
            InitializeComponent();


            buttonA.Clicked += async (sender, e) =>
            {
                await App.NavigateMasterDetail(new PaginaA());
            };

        }
    }
}
